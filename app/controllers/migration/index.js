const express = require("express");
const res = require("express/lib/response");
const mysql = require("mysql");
const path = require("path");
const router = express.Router();

router.use(express.json());

router.get("/client", (req, resp) => {
  var XLSX = require("xlsx");
  var workbook = XLSX.readFile(
    path.join(__dirname, "arquivos", "cliente_ori.xlsx")
  );
  var sheet_name_list = workbook.SheetNames;

  var arrayClientes = [];

  sheet_name_list.forEach(function (y) {
    var worksheet = workbook.Sheets[y];
    var headers = {};
    var data = [];
    for (z in worksheet) {
      if (z[0] === "!") continue;
      //parse out the column, row, and value
      var tt = 0;
      for (var i = 0; i < z.length; i++) {
        if (!isNaN(z[i])) {
          tt = i;
          break;
        }
      }
      var col = z.substring(0, tt);
      var row = parseInt(z.substring(tt));
      var value = worksheet[z].v;

      //store header names
      if (row == 1 && value) {
        headers[col] = value;
        continue;
      }

      if (!data[row]) data[row] = {};
      data[row][headers[col]] = value;
    }
    //drop those first two rows which are empty
    data.shift();
    data.shift();

    arrayClientes = data;
    console.log(data);
  });

  const connection = mysql.createConnection({
    host: "127.0.0.1",
    port: 3309,
    user: "root",
    password: "dominus2011",
    database: "excellent",
    insecureAuth: true,
    multipleStatements: true,
  });

  for (let index = 0; index < arrayClientes.length; index++) {
    const cliente = arrayClientes[index];

    let query2 = `Insert into cad_cliente_fornecedor (Id, Pesseoa_J_F, tipo_cliente_fornecedor,
                  status, DT_Cad, Nome_Fantasia, CNPJ_CPF, Fone_Cel, Endereco_rua_Av, Bairro, 
                  UF, Razao_social_SobreNome, Num_Endereco, CEP) values ("${cliente.CODIGO}","Fisica",
                  "Cliente", "Ativo", "2022-02-21", "${cliente.NOME}", "${cliente.CPF}", "${String(cliente.CELULAR).substring(0, 14)}",
                  "${cliente.ENDERECO}", "${cliente.BAIRRO}","PR", "${String(cliente.NOME).substring(0, 60)}","${cliente.NUMERO}","86860000");`;

    connection.query(query2, (error, results, fields) => {
      if (error) {
        console.error(error.message);
        return true;
      }

      if (index == arrayClientes.length - 1) {
        resp.json({ message: "Foi um sucesso" });
      }
    });
  }

  connection.end();
});

  router.get("/product", (req, resp) => {
  var XLSX = require("xlsx");
  var workbook = XLSX.readFile(
    path.join(__dirname, "arquivos", "produtos_ori.xlsx")
  );
  var sheet_name_list = workbook.SheetNames;

  var arrayProdutos = [];

  sheet_name_list.forEach(function (y) {
    var worksheet = workbook.Sheets[y];
    var headers = {};
    var data = [];
    for (z in worksheet) {
      if (z[0] === "!") continue;
      //parse out the column, row, and value
      var tt = 0;
      for (var i = 0; i < z.length; i++) {
        if (!isNaN(z[i])) {
          tt = i;
          break;
        }
      }
      var col = z.substring(0, tt);
      var row = parseInt(z.substring(tt));
      var value = worksheet[z].v;

      //store header names
      if (row == 1 && value) {
        headers[col] = value;
        continue;
      }

      if (!data[row]) data[row] = {};
      data[row][headers[col]] = value;
    }
    //drop those first two rows which are empty
    data.shift();
    data.shift();

    arrayProdutos = data;
    console.log(data);
  });

  const connection = mysql.createConnection({
    host: "127.0.0.1",
    port: 3309,
    user: "root",
    password: "dominus2011",
    database: "excellent",
    insecureAuth: true,
    multipleStatements: true,
  });

  for (let index = 0; index < arrayProdutos.length; index++) {
    const produto = arrayProdutos[index];

    let price = String(produto.PRECOVENDA).replace("000", "")
    let priceV = price == "" ? "0": String(price) == "undefined"? "0": price 

    let query2 =`Insert into cad_produtos (Id, Descricao, Preco_Venda, Num_Tamanho, Preco_Venda_aP,Qtde_Atual,Id_Unidade_Media, Controla_Estoque,
                 NCM, Id_tributo, Tipo_Produto,ATpVenda, ativo) values ("${produto.CODIGO}","${String(produto.DESCRICAO).substring(0, 60)}",${priceV},
                 "${produto.UNIDADE}",${priceV},"0","1","True","${produto.NCM}","1","P","True","A");`;

    connection.query(query2, (error, results, fields) => {
      if (error) {
        console.error(error.message);
        return true;
      }

      if (index == arrayProdutos.length - 1) {
        resp.json({ message: "Foi um sucesso" });
      }
    });
  }

  connection.end();
});

router.get("/venda", (req, resp) => {
  var XLSX = require("xlsx");
  var workbook = XLSX.readFile(
    path.join(__dirname, "arquivos", "produtos_ori.xlsx")
  );
  var sheet_name_list = workbook.SheetNames;

  var arrayVenda = [];

  sheet_name_list.forEach(function (y) {
    var worksheet = workbook.Sheets[y];
    var headers = {};
    var data = [];
    for (z in worksheet) {
      if (z[0] === "!") continue;
      //parse out the column, row, and value
      var tt = 0;
      for (var i = 0; i < z.length; i++) {
        if (!isNaN(z[i])) {
          tt = i;
          break;
        }
      }
      var col = z.substring(0, tt);
      var row = parseInt(z.substring(tt));
      var value = worksheet[z].v;

      //store header names
      if (row == 1 && value) {
        headers[col] = value;
        continue;
      }

      if (!data[row]) data[row] = {};
      data[row][headers[col]] = value;
    }
    //drop those first two rows which are empty
    data.shift();
    data.shift();

    arrayVenda = data;
    console.log(data);
  });

  const connection = mysql.createConnection({
    host: "127.0.0.1",
    port: 3309,
    user: "root",
    password: "dominus2011",
    database: "excellent",
    insecureAuth: true,
    multipleStatements: true,
  });

  for (let index = 0; index < arrayVenda.length; index++) {
    const venda = arrayVenda[index];


    let query2 =`Insert into `

    connection.query(query2, (error, results, fields) => {
      if (error) {
        console.error(error.message);
        return true;
      }

      if (index == arrayVenda.length - 1) {
        resp.json({ message: "As vendas foram um sucesso" });
      }
    });
  }

  connection.end();
});

module.exports = router;
